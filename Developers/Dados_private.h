/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef DADOS_PRIVATE_H
#define DADOS_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"0.1.1.1"
#define VER_MAJOR	0
#define VER_MINOR	1
#define VER_RELEASE	1
#define VER_BUILD	1
#define COMPANY_NAME	"Sancho"
#define FILE_VERSION	"Alpha"
#define FILE_DESCRIPTION	"Developed using the Dev-C++ IDE"
#define INTERNAL_NAME	"Generador Vampiro"
#define LEGAL_COPYRIGHT	""
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	""
#define PRODUCT_NAME	""
#define PRODUCT_VERSION	""

#endif /*DADOS_PRIVATE_H*/
